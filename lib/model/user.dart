import 'package:intl/intl.dart';


class User{
  static String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
  String username;
  String email;
  DateTime created_at;


  User(this.username, this.email, this.created_at);

  User.fromJson(Map<String, dynamic> json)
    : username = json['username'],
      email = json['email'],
      created_at = DateFormat(dateFormat,"fr_FR")
        .parse(json['created_at'],true)
  ;

  Map<String, dynamic> toJson() =>
    {
      'username': username,
      'email': email,
      'created_at': DateFormat(dateFormat,"fr_FR").format(created_at)
    };
}