import 'package:ynov_chat_cours/model/user.dart';
import 'package:intl/intl.dart';

class Message{
  static String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
  num id;
  String content;
  User author;
  DateTime published_at;
  bool isImage;


  Message(this.id, this.content, this.author, this.published_at, this.isImage);

  Message.fromJson(Map<String, dynamic> json)
    : id = json['id'],
      content = json['content'],
      author = User.fromJson(json['author']),
      isImage = json.containsKey("isImage") ? json['isImage'] : false,
      published_at = DateFormat(dateFormat,"fr_FR")
        .parse(json['published_at'],true)
  ;

  Map<String, dynamic> toJson() =>
    {
      'id': id.toString(),
      'content': content,
      'author': author.toJson(),
      'isImage' : isImage.toString(),
      'published_at': DateFormat(dateFormat,"fr_FR").format(published_at)
    };
}