import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ynov_chat_cours/page/detail_media_page.dart';
import 'package:ynov_chat_cours/page/home_page.dart';
import 'package:ynov_chat_cours/page/login_page.dart';
import 'package:ynov_chat_cours/page/register_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  static const ROUTE_LOGIN = '/loginPage';
  static const ROUTE_REGISTER = '/registerPage';
  static const ROUTE_HOME = '/homePage';
  static const ROUTE_DETAIL_MEDIA = '/detailMedia';
  static final FlutterSecureStorage storage = new FlutterSecureStorage();
  static const String KEY_JWT = "jwt";
  static const String KEY_USERNAME = "username";

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //Sélectionne le thème à utiliser
      themeMode: ThemeMode.light,
      //On spécifie les particularités du thème dark
      darkTheme: ThemeData(
        brightness: Brightness.dark
      ),
      initialRoute: ROUTE_LOGIN,
      onGenerateRoute: (RouteSettings settings){
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context){
            switch(settings.name){
              case ROUTE_LOGIN:
                return LoginPage();
              case ROUTE_REGISTER:
                return RegisterPage();
              case ROUTE_HOME:
                return MyHomePage(title: "YnovCours",);
              case ROUTE_DETAIL_MEDIA:
                return DetailMediaPage(
                  (settings.arguments as List )[0],
                  (settings.arguments as List )[1]);
              default:
                return Center(child: Text("404 : Page Not Found"));
            }
          }
        );
      },
      /*
      routes: <String, WidgetBuilder> {
        ROUTE_LOGIN: (BuildContext context) => LoginPage(),
        ROUTE_REGISTER: (BuildContext context) => RegisterPage(),
        ROUTE_HOME: (BuildContext context) => MyHomePage(title: "YnovCours",),
        ROUTE_DETAIL_MEDIA: (BuildContext context) => DetailMediaPage(imgSrc:),
      },*/
    );
  }
}

