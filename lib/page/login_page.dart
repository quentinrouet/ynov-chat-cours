import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:jwt_decoder/jwt_decoder.dart';

import '../main.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _textEditContIdentifier = TextEditingController();
  TextEditingController _textEditContPassword = TextEditingController();


  @override
  void initState() {
    _checkIsAlreadyConnected();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Connexion YnovChat"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Stack(
            children: [
              //1
              Align(
                alignment: AlignmentDirectional.topCenter,
                child: Text(
                    "Connectez-vous à YnovChat",
                    style: Theme.of(context).textTheme.headline6,
                  ),
              ),
              //2
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _buildListFields(),
              ),
              //3
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: _buildListButtons(),
              )
            ],
          ),
        )
      ),
    );
  }

  List<Widget> _buildListFields(){
    return [
      TextFormField(
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.email),
          hintText: "Nom d'utilisateur ou e-mail"
        ),
        controller: _textEditContIdentifier,
        validator: (value){
          return
            value.isEmpty || value.length < 2
              ? "le champs doit avoir plus de 1 caractère"
              : null;
        },
      ),
      TextFormField(
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.lock),
          hintText: "Mot de passe"
        ),
        obscureText: true,
        controller: _textEditContPassword,
        validator: (value){
          return value.isEmpty ? "Le champs est vide" : null;
        },

      ),
    ];
  }

  List<Widget> _buildListButtons(){
    return [
      Center(
        child: Container(
          width: 150,
          child: ElevatedButton(
            onPressed: (){
              if(_formKey.currentState.validate()){
                //TODO Login Server
                _sendLoginRequest();
              }
            }
            , child: Text("Se connecter"),
          ),
        )
      ),
      Center(
        child: MaterialButton(
          onPressed: (){
            //TODO Naviguer vers s'enregistrer
            Navigator.of(context).pushReplacementNamed(MyApp.ROUTE_REGISTER);
          }
          , child: Text("S'enregister"),
        )
      ),
    ];
  }

  Future<void> _sendLoginRequest() async{
    http.Response responseRegister = await http.post(
      Uri.parse("https://flutter-learning.mooo.com/auth/local/"),
      body: {
        "identifier":_textEditContIdentifier.value.text,
        "password":_textEditContPassword.value.text
      }
    );
    if(responseRegister.statusCode == 200){
      //récupérer et enregistrer le token
      Map<String,dynamic> responseBody = jsonDecode(responseRegister.body);
      MyApp.storage.write(key: MyApp.KEY_JWT, value: responseBody["jwt"]);
      MyApp.storage.write(key: MyApp.KEY_USERNAME, value: responseBody["user"]["username"]);

      Navigator.of(context).pushReplacementNamed(MyApp.ROUTE_HOME);
    } else{
      //affiche message erreur
      ScaffoldMessenger.of(context).showSnackBar(
        new SnackBar(content: Text("Erreur lors de la connexion"))
      );
    }
  }

  Future<void> _checkIsAlreadyConnected() async{
    if(await MyApp.storage.containsKey(key: MyApp.KEY_JWT)){
      String jwt = await MyApp.storage.read(key: MyApp.KEY_JWT);
      if(! JwtDecoder.isExpired(jwt))
        Navigator.of(context).pushReplacementNamed(MyApp.ROUTE_HOME);
    }
  }
}
