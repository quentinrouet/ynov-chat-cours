import 'dart:convert';
import 'dart:developer' as developer;
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:timeago/timeago.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:ynov_chat_cours/main.dart';
import 'package:ynov_chat_cours/model/message.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key,this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();

}

class _MyHomePageState extends State<MyHomePage> {
  List<Message> listMessages;
  Stream<List<Message>> _streamMessages;
  StreamController<List<Message>> _streamControllerMessages;
  File _imageSelected;
  final picker = ImagePicker();
  final _textEditContMsg = new TextEditingController();
  final _focusNodeMsg = new FocusNode();
  final _formKey = new GlobalKey<FormState>();
  final _scrollControllList = new ScrollController();

  @override
  void initState() {
    initializeDateFormatting("fr_FR");
    setLocaleMessages('fr', FrShortMessages());
    _streamControllerMessages = new StreamController<List<Message>>();
    _streamMessages = _streamControllerMessages.stream;
    _fetchMessages();
  }

  @override
  void dispose() {
    super.dispose();
    _streamControllerMessages.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          //TODO refresh manuel
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: (){
              _fetchMessages();
            }),
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: (){
              MyApp.storage.delete(key: MyApp.KEY_JWT);
              Navigator.of(context).pushReplacementNamed(MyApp.ROUTE_LOGIN);
          })
        ],
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(child: _buildListMessages()),
          _buildFormMessages()
        ],
      )
    );
  }

  Widget _buildFormMessages() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        PopupMenuButton(
          onSelected:(itemSelected) async {
            switch(itemSelected){
              case 0:
                //TODO récupération position
                _determinePosition().then((value) {
                  num _lat = value.latitude;
                  num _lng = value.longitude;
                  ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text("position: $_lat : $_lng")));
                  _textEditContMsg.text = "$_lat, $_lng";
                },onError: (error){
                  ScaffoldMessenger.of(context)
                    .showSnackBar(SnackBar(content: Text("erreur : $error")));
                });

                break;
              case 1:
                getImage();
                break;
              default:
                break;
            }
          },
          itemBuilder: (context) => <PopupMenuEntry>[
            PopupMenuItem(
              value: 0,
              child: ListTile(
                leading: Icon(Icons.location_on),
                title: Text("Partager sa localisation"),
              ),
            ),
            PopupMenuDivider(height: 8,),
            PopupMenuItem(
              value: 1,
              child: ListTile(
                leading: Icon(Icons.photo),
                title: Text("Envoyer une image"),
              ),
            )
          ],
          icon: Icon(Icons.add),
        ),
        Form(
          key: _formKey,
          child: Expanded(
            child: TextFormField(
              focusNode: _focusNodeMsg,
              controller: _textEditContMsg,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left:8.0),
                hintText: "Envoyez votre message"
              ),
              validator: (valeur){
                return valeur.isEmpty ? "Veuillez rentrer un message" : null;
              },
            ),
          ),
        ),
        IconButton(icon: Icon(Icons.send),onPressed:(){
          _sendMessage();
        })
      ],
    );
  }

  Widget _buildListMessages(){
    return StreamBuilder<List<Message>>(
      stream: _streamMessages,
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.active &&
          snapshot.hasData){
          listMessages = snapshot.data;
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            _scrollToBottomList();
          });
          return ListView.builder(
            controller: _scrollControllList,

            itemCount: listMessages.length,
            itemBuilder: (context, index) {
              return ListTile(
                //onLongPress: _onTapMessage(),
                leading:
                  Container(width: 48, height: 48,
                    child: CircleAvatar(
                      backgroundColor:
                        Colors.primaries[Random().nextInt(Colors.primaries.length)],
                      child: Text(listMessages[index].author.username.substring(0,3).toUpperCase()),
                    ),),
                title: prepareContentToShow(
                  listMessages[index].content,
                  isImage : listMessages[index].isImage,
                  author: listMessages[index].author.username
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Hero(
                      tag: "tagAuthor",
                      child: Text(listMessages[index].author.username)
                    ),
                    Text(format(listMessages[index].published_at,locale: 'fr')),
                  ],
                ),
              );
            }
          );
        } else
          return Center(child: CircularProgressIndicator());
      }
    );
  }

  void _onTapMessage(){

  }

  Widget prepareContentToShow(String content, {bool isImage, String author}){
    String rawRegexTel = r"^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$";
    String rawRegexGeo = r"^(-?\d+(\.\d+)?),\s*(-?\d+(\.\d+)?)$";

    if(RegExp(rawRegexTel).hasMatch(content)){
      return GestureDetector(
        onTap: () => launch("tel:" + content),
        //
        child: Text("📞 Appuyer ici pour appeler le numéro suivant : $content")
      );
    }
    else if(RegExp(rawRegexGeo).hasMatch(content)){
      return GestureDetector(
        onTap: () => launch("geo:" + content),
        //
        child: Text("📍 Appuyer ici pour accéder à la localisation de l'utilisateur $content")
      );
    }
    else if(isImage)
      try{
        return GestureDetector(
          onTap: (){
            Navigator.of(context).pushNamed(
              MyApp.ROUTE_DETAIL_MEDIA,
              arguments: [base64Decode(content), author]);
          },
          child: Hero(
            tag: base64Decode(content),
            child: Image.memory(base64Decode(content))
          )
        );
      }on FormatException catch(formatException){
        developer.log("Affichage image : " + formatException.message);
        return Image.asset("images/corrupted_image.png");
      }catch (allOtherExceptions){
        developer.log("Affichage image : " + allOtherExceptions.message);
      }
    else
      return Text(content);
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery,imageQuality: 1);
    if (pickedFile != null) {
      _imageSelected = File(pickedFile.path);
      String uriBase64 = base64Encode(_imageSelected.readAsBytesSync());
      _sendMessage(isImage: true, uribase64: uriBase64);

    } else {
      print('No image selected.');
    }
  }

  _scrollToBottomList(){
    _scrollControllList.animateTo(
      _scrollControllList.position.maxScrollExtent,
      curve: Curves.linear,
      duration: Duration(seconds: 1),
    );
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
  }

  /**
   * REQUESTS API
   */
  Future<void> _fetchMessages() async{
    http.Response responseMessages = await http.get(
      Uri.parse(
        "https://flutter-learning.mooo.com/api/messages?_limit=-1"),
        headers: {
          "Authorization" : "Bearer " + await MyApp.storage.read(key: MyApp.KEY_JWT)
        }
    );
    //TODO check 200
    if(responseMessages.statusCode == 200){
      List listMsgJSON = jsonDecode(responseMessages.body) as List;
      List<Message> listMessages =
      listMsgJSON.map((e) => Message.fromJson(e)).toList();
      _streamControllerMessages.add(listMessages);

    } else
      _streamControllerMessages.addError("Rééssayez plus tard 🙁");
  }

  Future<void> _sendMessage({final bool isImage = false, final String uribase64}) async{
    if(_formKey.currentState.validate() || isImage){
      //if(isImage)
      //_textEditContMsg.text = "🌄 Image sélectionnée";
      http.Response responseMessages = await http.post(
        Uri.parse(
          "https://flutter-learning.mooo.com/api/messages"),
        headers: {
          "Authorization" : "Bearer " + await MyApp.storage.read(key: MyApp.KEY_JWT)
        },
        body:{
          "content" : isImage ? uribase64 : _textEditContMsg.value.text,
          "isImage" : jsonEncode(isImage)
        }
      );
      if(responseMessages.statusCode == 200){
        _textEditContMsg.clear();
        _focusNodeMsg.unfocus();
        //TODO Aller en bas de la liste

        _fetchMessages();
      }
    }
  }



}
