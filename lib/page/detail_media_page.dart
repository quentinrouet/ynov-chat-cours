import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DetailMediaPage extends StatefulWidget {
  final Uint8List imgSrc;
  final String author;

  DetailMediaPage(this.imgSrc, this.author);

  @override
  _DetailMediaPageState createState() => _DetailMediaPageState();
}

class _DetailMediaPageState extends State<DetailMediaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Affichage Image"),),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Hero(
            tag: widget.imgSrc,
            child: Image.memory(widget.imgSrc)
          ),
          Hero(
            tag: "tagAuthor",
            child: Text(widget.author))
        ],
      ),
    );
  }
}
