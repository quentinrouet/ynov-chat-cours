import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:ynov_chat_cours/main.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController
  _textEditContEmail = TextEditingController(),
  _textEditContPassword = TextEditingController(),
  _textEditContUsername = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Inscription YnovChat"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Stack(
            children: [
              //1
              Align(
                alignment: AlignmentDirectional.topCenter,
                child: Text(
                  "Inscrivez-vous à YnovChat",
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              //2
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _buildListFields(),
              ),
              //3
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: _buildListButtons(),
              )
            ],
          ),
        )
      ),
    );
  }

  List<Widget> _buildListFields(){
    return [
      TextFormField(
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.email),
          hintText: "E-mail"
        ),
        controller: _textEditContEmail,
        validator: (value){
          return
            (value.isEmpty || !value.contains("@") || (value.length < 2))
              ? "Adresse e-mail incorrecte"
              : null;
        },
      ),
      TextFormField(
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.person),
          hintText: "Nom d'utilisateur"
        ),
        controller: _textEditContUsername,
        validator: (value){
          return
            value.isEmpty || value.length < 2
              ? "le champs doit avoir plus de 1 caractère"
              : null;
        },
      ),
      TextFormField(
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.lock),
          hintText: "Mot de passe"
        ),
        obscureText: true,
        controller: _textEditContPassword,
        validator: (value){
          return value.isEmpty ? "Le champs est vide" : null;
        },

      ),
    ];
  }

  List<Widget> _buildListButtons(){
    return [
      Center(
        child: Container(
          width: 150,
          child: ElevatedButton(
            onPressed: (){
              if(_formKey.currentState.validate()){
                //TODO register Server
                _sendRegisterRequest();
              }
            }
            ,child: Text("S'inscrire"),
          ),
        )
      ),
      Center(
        child: MaterialButton(
          onPressed: (){
            //TODO Naviguer vers se connecter
            Navigator.of(context).pushReplacementNamed(MyApp.ROUTE_LOGIN);
          }
          , child: Text("Se connecter"),
        )
      ),
    ];
  }
  
  Future<void> _sendRegisterRequest() async{
    http.Response responseRegister = await http.post(
      Uri.parse("https://flutter-learning.mooo.com/auth/local/register"),
      body: {
        "email":_textEditContEmail.value.text,
        "username":_textEditContUsername.value.text,
        "password":_textEditContPassword.value.text
      }
    );
    if(responseRegister.statusCode == 200){
      //récupérer et enregistrer le token
      Map<String,dynamic> responseBody = jsonDecode(responseRegister.body);
      MyApp.storage.write(key: MyApp.KEY_JWT, value: responseBody["jwt"]);
      MyApp.storage.write(key: MyApp.KEY_USERNAME, value: responseBody["user"]["username"]);

      Navigator.of(context).pushReplacementNamed(MyApp.ROUTE_HOME);
    } else{
      //affiche message erreur
      ScaffoldMessenger.of(context).showSnackBar(
        new SnackBar(content: Text("Erreur lors de l'inscription"))
      );
    }
  }
}
